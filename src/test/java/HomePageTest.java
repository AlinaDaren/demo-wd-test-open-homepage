import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomePageTest {

    @Test
    public void testOnlinerOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.onliner.by/");

        String locator = "//img[@class='onliner_logo']";
        By byLocator = By.xpath(locator);
        WebElement logoElement = driver.findElement(byLocator);
        //WebElement logoElement = driver.findElement(By.xpath("//img[@class='onliner_logo']"));

        Assert.assertTrue(logoElement.isDisplayed());

        driver.quit();
    }

    @Test
    public void testAmazonOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.amazon.com/");

        /*String locator = "//*[@id='nav-logo-sprites']";
        By byLocator = By.xpath(locator);
        WebElement logoElement = driver.findElement(byLocator);*/

        //WebElement logoElement = driver.findElement(By.xpath("//*[@id='nav-logo-sprites']"));
        WebElement logoElementId = driver.findElement(By.id("nav-logo-sprites")); //tried to use "findElement(By.id)"

        Assert.assertTrue(logoElementId.isDisplayed());
        driver.quit();
    }

    @Test
    public void testTicketProOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.ticketpro.by/");

        String locator = "//img[@src='/build/img/logo.png']";
        By byLocator = By.xpath(locator);
        WebElement logoElement = driver.findElement(byLocator);

        //WebElement logoElement = driver.findElement(By.xpath("//img[@src='/build/img/logo.png']"));

        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testAlatanTurOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://alatantour.by/");

        String locator = "//img[@alt='Алатан Тур']";
        By byLocator = By.xpath(locator);
        WebElement logoElement = driver.findElement(byLocator);

        //WebElement logoElement = driver.findElement(By.xpath("//img[@alt='Алатан Тур']"));

        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testOLXOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.olx.pl/");
        //WebElement logoElement = driver.findElement(By.xpath("//*[@id='headerLogo']"));

        WebElement logoElement = driver.findElement(By.id("headerLogo"));

        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

    @Test
    public void testTripAdvisorOpen() {

        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.tripadvisor.com/");

        WebElement logoElement = driver.findElement(By.xpath("//img[@alt='Tripadvisor']"));

        Assert.assertTrue(logoElement.isDisplayed());
        driver.quit();
    }

}
